# Models Licensing

## reel.glb

[Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/)

### Author (for attribution)

Karen Gharibyan  
[karen.gharibyan3@gmail.com](karen.gharibyan3@gmail.com)  
[https://michaelkolesidis.com](https://michaelkolesidis.com)

## button.glb

[Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/)

### Author (for attribution)

Karen Gharibyan  
[karen.gharibyan3@gmail.com](karen.gharibyan3@gmail.com)  
[https://michaelkolesidis.com](https://michaelkolesidis.com)
