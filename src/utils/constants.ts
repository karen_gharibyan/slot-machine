// Copyright (c) 2023 Karen Gharibyan <karen.gharibyan3@gmail.com>
// Licensed under the GNU Affero General Public License v3.0.
// https://www.gnu.org/licenses/gpl-3.0.html

export const WHEEL_SEGMENT = Math.PI / 4;
